#ifndef USERSPACE
#define USERSPACE
#include "quantum.h"
#include "keymap_bepo.h"

void update_led(void);

// Layer name
enum layers {
  BEPO = 0,
  ADJUST,
  LOWER,
  RAISE,
  FN,
  GAME,
};

// Custom keycode, for macro
enum custom_keycodes {
  COPY = SAFE_RANGE,
  PASTE,
  CUT,
  CAPSLOCK,
  ALT_TAB,
  LED,
  MY_DLR,
  MY_PERC,
};

//Tap Dance Declarations
enum {
  comma_semicolon,
  a_aaccent,
  i_trema,
};

// color palette for LED
typedef enum {
  WHITE = 0,
  RED,
  BLUE,
  GREEN,
  YELLOW,
  OFF,
} color_e;

typedef struct
{
  uint16_t my_keycode;
  uint16_t key_normal;
  uint16_t key_caps;
  bool     overide_in_capslock;
} overide_caps_t;

#ifndef MOUSEKEY_INTERVAL
#define MOUSEKEY_INTERVAL 16
#endif
#define MOUSEKEY_DELAY 0
#define MOUSEKEY_TIME_TO_MAX 60
#define MOUSEKEY_MAX_SPEED 7
#define MOUSEKEY_WHEEL_DELAY 0

#define LED_LEVEL   6

#define _______ KC_TRNS
#define xxxxxxx KC_NO

// Leader key macro
#define TMUX    LCTL(BP_B)
#define VIM     LCTL(BP_W)
#define PC_LOCK LALT(LCTL(BP_L))

#define BEPO_LEFT_NB   BP_DLR,             BP_DQOT, MY_PERC, KC_ESC, S(KC_4),  S(KC_5)
#define BEPO_LEFT_01   LT(ADJUST,KC_TAB),  BP_B,    BP_ECUT, BP_P,   BP_O,     SFT_T(BP_EGRV)
#define BEPO_LEFT_02   BP_W,  TD(a_aaccent),  BP_U,  BP_I,  BP_E,  TD(comma_semicolon)
#define BEPO_LEFT_03   KC_LCTRL,           BP_Z,    BP_Y,    BP_X,   BP_DOT,   BP_K
#define BEPO_LEFT_TH   MO(LOWER),          KC_BSPC, KC_DEL

#define BEPO_RIGHT_NB  BP_AT,          BP_PLUS,  BP_MINS,  BP_SLSH,  BP_ASTR,  BP_EQL
#define BEPO_RIGHT_01  SFT_T(BP_DCRC), BP_V,     BP_D,     BP_L,     BP_J,     LT(FN,KC_CAPS)
#define BEPO_RIGHT_02  BP_C, BP_T,     BP_S,     BP_R,     BP_N,     BP_M
#define BEPO_RIGHT_03  BP_APOS,        BP_Q,     BP_G,     BP_H,     BP_F,     KC_RCTL
#define BEPO_RIGHT_TH  KC_ENT,         KC_SPC,   MO(RAISE)


// btn2 is swaped with btn3 ? (at work at least)
#define RAISE_LEFT_NB  _______,  _______,  BP_LESS,  BP_GRTR,  _______,  _______
#define RAISE_LEFT_01  _______,  BP_PIPE,  BP_LBRC,  BP_RBRC,  _______,  _______
#define RAISE_LEFT_02  KC_BTN3,  BP_AMPR,  BP_LPRN,  BP_RPRN,  BP_EURO,  _______
#define RAISE_LEFT_03  _______,  BP_BSLS,  BP_LCBR,  BP_RCBR,  BP_UNDS,  BP_TILD
#define RAISE_LEFT_TH  _______,  _______,  _______

#define RAISE_RIGHT_NB _______,  BP_PSMS,  BP_MMNS,  BP_OBEL,  BP_TIMES, BP_DIFF
#define RAISE_RIGHT_01 _______,  TMUX,     KC_UP,    PC_LOCK,  _______,  TG(GAME)
#define RAISE_RIGHT_02 BP_CCED,  KC_LEFT,  KC_DOWN,  KC_RGHT,  _______,  _______
#define RAISE_RIGHT_03 _______,  _______,  BP_DGRK,  _______,  _______,  _______
#define RAISE_RIGHT_TH _______,  _______,  _______


#define LOWER_LEFT_NB  _______,  S(KC_1),  S(KC_2),  S(KC_3),  S(KC_4),  S(KC_5)
#define LOWER_LEFT_01  ALT_TAB,  BP_AE,    BP_DACT,  _______,  BP_OE,    BP_DGRV
#define LOWER_LEFT_02  KC_VOLU,  BP_AGRV,  BP_UGRV,  BP_DTRM,  _______,  BP_TAPO
#define LOWER_LEFT_03  KC_VOLD,  KC_MPLY,  KC_MNXT,  _______,  _______,  _______
#define LOWER_LEFT_TH  _______,  _______,  _______

#define LOWER_RIGHT_NB S(KC_6),  S(KC_7),  S(KC_8),  S(KC_9),  S(KC_0),  LED
#define LOWER_RIGHT_01 _______,  TMUX,     KC_UP,    VIM,      _______,  _______
#define LOWER_RIGHT_02 KC_PGUP,  KC_LEFT,  KC_DOWN,  KC_RGHT,  KC_PGDN,  _______
#define LOWER_RIGHT_03 _______,  KC_HOME,  _______,  KC_END,   _______,  _______
#define LOWER_RIGHT_TH _______,  BP_UNDS,  _______


#define ADJUST_LEFT_NB   _______,  _______,  _______,  _______,  _______,  _______
#define ADJUST_LEFT_01   _______,  _______,  _______,  _______,  _______,  _______
#define ADJUST_LEFT_02   _______,  _______,  BP_LPRN,  BP_RPRN,  _______,  BP_COMM
#define ADJUST_LEFT_03   _______,  _______,  _______,  _______,  _______,  _______
#define ADJUST_LEFT_TH   _______,  _______,  _______

#define ADJUST_RIGHT_NB  _______,  BP_PLUS,  BP_MINS,  BP_SLSH,  BP_ASTR,  BP_EQL
#define ADJUST_RIGHT_01  _______,  KC_P7,    KC_P8,    KC_P9,    KC_P0,    KC_TAB
#define ADJUST_RIGHT_02  _______,  KC_P4,    KC_P5,    KC_P6,    _______,  _______
#define ADJUST_RIGHT_03  _______,  KC_P1,    KC_P2,    KC_P3,    _______,  _______
#define ADJUST_RIGHT_TH  _______,  _______,  MO(RAISE)

#define FN_LEFT_NB   KC_F12,   KC_F1,    KC_F2,    KC_F3,    KC_F4,    KC_F5
#define FN_LEFT_01   _______,  _______,  KC_BTN2,  KC_MS_U,  KC_BTN1,  _______
#define FN_LEFT_02   _______,  _______,  KC_MS_L,  KC_MS_D,  KC_MS_R,  _______
#define FN_LEFT_03   _______,  _______,  _______,  _______,  _______,  _______
#define FN_LEFT_TH   _______,  _______,  _______

#define FN_RIGHT_NB  KC_F6,    KC_F7,    KC_F8,    KC_F9,    KC_F10,   KC_F11
#define FN_RIGHT_01  _______,  _______,  _______,  _______,  _______,  _______
#define FN_RIGHT_02  _______,  _______,  _______,  _______,  _______,  _______
#define FN_RIGHT_03  _______,  _______,  _______,  _______,  _______,  _______
#define FN_RIGHT_TH  _______,  _______,  _______

#define GAME_LEFT_NB   KC_RALT,  KC_1,     KC_2,     KC_3,     KC_4,     KC_5
#define GAME_LEFT_01   _______,  _______,  _______,  _______,  _______,  _______
#define GAME_LEFT_02   _______,  BP_A,     _______,  BP_I,     _______,  _______
#define GAME_LEFT_03   _______,  _______,  _______,  _______,  _______,  _______
#define GAME_LEFT_TH   _______,  _______,  _______

#define GAME_RIGHT_NB  KC_6,     KC_7,     KC_8,     KC_9,     KC_0,     LED
#define GAME_RIGHT_01  _______,  _______,  _______,  _______,  _______,  TG(GAME)
#define GAME_RIGHT_02  _______,  _______,  _______,  _______,  _______,  _______
#define GAME_RIGHT_03  _______,  _______,  _______,  _______,  _______,  _______
#define GAME_RIGHT_TH  _______,  _______,  _______

#endif
