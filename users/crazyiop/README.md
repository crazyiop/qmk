# wBépo layout

This layout is based on the french [bépo](https://bepo.fr/wiki/Accueil) layout, which I have been using since 2010. It includes the changes and tweaks I gradually implemented after using an ergodox for ~1 year, slowly taking advantage of qmk's features. I cleaned the whole thing after buying a preonic, to easily maintain a similar base layout on different hardware.

## reason of changes from the bépo layout
I have made a few change of the bépo layout to accomodate fewer keys keyboard (60%). As a programmer, I also, changed some key as I find them useless to me ('«»' for example).

But as the bépo layout is standarized, and in xorg xkb (`setxkbmap fr bepo`) I also wanted a keyboard that function on any unix machine, that use that pc-side software layout to gives me easy access to french specific character. i.e all my works will be inside the keyboard, I don't want to touch the pc's layout definition or create my own.

To not use the bépo/bepo name and create confusion, I named my layout wbepo as it's biggest change is to move the w from the far right to the far left.

### keys removed
To use on smaller keyboard, I have removed a few keys that are not that much used (certainly a personal bias here):
- Ç : moved on alt-gr c, replacing the useless copyright symbol
- À : can now be done with the above dead accent

### W Z
For symetry, the W and Z needed to move.
- Z takes the place of the old À
- W takes the place on the left of the A (caps lock on normal keyboard)

### «»
The french guillemets are removed. By habits, < and > are left in alt-gr, but that still free two direct acces key for the top layer ones that get's removed in the 60% : ESC and %. This is also a better ESC place as I'm a vim user, and use it a lot.

## layer
I use the now classic raise/lower layer name. The only difference is that I don't like to have both thumb locked for typing anything else, so my adapt layer is activated by its own key.

On the bépo layout, alt-gr has a frequent use for some symbols. As I wanted to do some change on the alt-gr 'layer' too, I will use the raise layer that will replicate most of the alt-gr modifier (same key placement). Note that in the end I do not have alt or alt-gr key at all. That might cause you some trouble in you are used to shortcut using them...

### base layer
 - shift keys are done by keeping è or ^ pressed.
 - alt-tab redone as macro with lower-tab

### raise (alt-gr alternative)
 - various symbols (|&\\~\_)
 - to uniform []() with {}<> which are a way more convenient
 - a few media key

### lower
 - directional keys (wasd style, I have never got used to the hjkl style)
 - movements: home, end, page up/down
 - tmux and vim, leader key
 - some french alt letter (ïçæœùè)

### fn
 - Functions keys F0-F12

### game
 This allow to shunt the few tapdance which induce latency to key registration
