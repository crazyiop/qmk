#include "crazyiop.h"
#include "version.h"
#include "action_layer.h"

// Must be defined in each keyboard, depending of its hardware
__attribute__ ((weak))
void set_led_color(color_e color) {};

__attribute__ ((weak))
void cycle_led_brigthness(void) {};


// Common LED indicator
void update_led(void) {
  // Capslock priority
  if (host_keyboard_leds() & (1<<USB_LED_CAPS_LOCK)) {
    set_led_color(WHITE);
  } else {
    // look layer...
    switch (biton32(layer_state)) {
      case RAISE:
        set_led_color(RED);
        break;
      case LOWER:
        set_led_color(BLUE);
        break;
      case ADJUST:
        set_led_color(GREEN);
        break;
      case FN:
      case GAME:
        set_led_color(YELLOW);
        break;
      case BEPO:
      default:
        set_led_color(OFF);
        break;
    }
  }
}

void led_set_user(uint8_t usb_led) {
  // must be triggered to
  // - activate capslock color
  // - go back to the proper layer color if needed when quitting capslock
  update_led();
}

uint32_t layer_state_set_user(uint32_t state) {
  // must be triggered to
  // - activate a layer color
  // - de-activate a layer color
  update_led();
  return state;
}

// Tap Dance Definitions
void dance_a(qk_tap_dance_state_t *state, void *user_data) {
  switch (state->count)
  {
    case 1:
      SEND_STRING("a");
      break;
    default:
      SEND_STRING(SS_TAP(X_Z)); // à
      break;
  }
  reset_tap_dance(state);
}

void dance_i(qk_tap_dance_state_t *state, void *user_data) {
  switch (state->count)
  {
    case 1:
      SEND_STRING("i");
      break;
    case 3:
      SEND_STRING(SS_RALT("i") "i"); // ï
    default:
      SEND_STRING(SS_RALT("i")); // "
      break;
  }
  reset_tap_dance(state);
}

qk_tap_dance_action_t tap_dance_actions[] = {
  // Tap once for first key, twice for second
  [comma_semicolon] = ACTION_TAP_DANCE_DOUBLE(BP_COMM, BP_SCLN),
  [a_aaccent] = ACTION_TAP_DANCE_FN(dance_a),
  [i_trema] = ACTION_TAP_DANCE_FN(dance_i),
};

// Override
overide_caps_t overide_tbl[] = {
  // my_keycode, key_normal, key_caps, overide_in_capslock
  {MY_DLR, BP_DLR, BP_A, true},
  {MY_PERC, BP_PERC, KC_P2, true}
};

// Common key processing
bool process_record_user(uint16_t keycode, keyrecord_t *record)
{
  // Persistant variable
  static bool in_tab = false; // does an ALT-TAB, for windows cycling, without an alt key

  for (int i = 0; i < (sizeof(overide_tbl)/sizeof(overide_caps_t)); i++) {
    if (keycode == overide_tbl[i].my_keycode) {
      // Handle a key that have its normal Shift key disociated
      if (record->event.pressed) {
        if (host_keyboard_leds() & (1<<USB_LED_CAPS_LOCK))
        {
          if (overide_tbl[i].overide_in_capslock)
          {
            register_code(overide_tbl[i].key_caps);
          } else {
            register_code(overide_tbl[i].key_normal);
          }
          return false;
        }

        if ((keyboard_report->mods & MOD_BIT(KC_LSFT))
            || (keyboard_report->mods & MOD_BIT(KC_RSFT))) {
          // Key press with shift -> override it
          register_code(overide_tbl[i].key_normal);
        } else {
          // Press the normal key
          register_code(overide_tbl[i].key_caps);
        }
        return false;
      }
    }
  }

  if ((keycode != ALT_TAB) && in_tab)
  {
    // Exit alt tab
    SEND_STRING(SS_UP(X_LALT));
    in_tab = false;
  }

  switch (keycode) {
    case ALT_TAB:
      // Macro to handle lower-tab as alt-tab
      if (record->event.pressed) {
        if (!in_tab)
        {
          SEND_STRING(SS_DOWN(X_LALT) SS_TAP(X_TAB));
          in_tab = true;
        } else {
          SEND_STRING(SS_TAP(X_TAB));
          // Do not release Alt here, or it will be impossible to switch more than one window:
          // alt-tab-tab will be interpreted as alt-tab, then tab
        }
      }
      return false;

    case KC_ESC:
      // Handle a key that have its normal Shift key disociated
      // Macro to have escape also desactivating caps lock if active
      if (record->event.pressed) {
        if (keyboard_report->mods & MOD_BIT(KC_LSFT)) {
          // Key press with shift -> override it for the correct number
          SEND_STRING(SS_TAP(X_3));
          return false;
        } else {
          // Not on shift -> esc
          if (host_keyboard_leds() & (1<<USB_LED_CAPS_LOCK)) {
            SEND_STRING(SS_TAP(X_CAPSLOCK));
          }
          return true;
        }
      }
      break;

    case COPY:
      if (record->event.pressed) {
        SEND_STRING(SS_LCTRL("c"));
      }
      return false;

    case PASTE:
      if (record->event.pressed) {
        SEND_STRING(SS_LCTRL("v"));
      }
      return false;

    case CUT:
      if (record->event.pressed) {
        SEND_STRING(SS_LCTRL("x"));
      }
      return false;

    case LED:
      // Dim led in cycle
      if (record->event.pressed) {
        cycle_led_brigthness();
      }
      return false;
  }

  // Process other keycodes normally
  return true;
}
