# My personal QMK layout

(Original QMK project and readme [here](https://github.com/qmk/qmk_firmware) )

## LAYOUT

My layout is commonize beetween an ergodox and a preonic keyboard, hence the split display.

### Base layer
![base](https://gitlab.com/crazyiop/qmk/raw/crazyiop/users/crazyiop/preonic-base-layer.jpg)

### Raise layer
![raise](https://gitlab.com/crazyiop/qmk/raw/crazyiop/users/crazyiop/preonic-raise-layer.jpg)

### Lower layer
![lower](https://gitlab.com/crazyiop/qmk/raw/crazyiop/users/crazyiop/preonic-lower-layer.jpg)

### Adjust layer
![adjust](https://gitlab.com/crazyiop/qmk/raw/crazyiop/users/crazyiop/preonic-adjust-layer.jpg)
