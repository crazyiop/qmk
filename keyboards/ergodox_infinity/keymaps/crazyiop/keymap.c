#include "crazyiop.h"
#include "keymap_bepo.h"
#include <sendstring_bepo.h>
#include "action_layer.h"
#include "version.h"
#include "ergodox_infinity.h"
#include "lcd_backlight.h"

#define _______ KC_TRNS
#define xxxxxx KC_NO

#define KEYMAP_custom(                                                                    \
    /* left hand, spatial positions */     /* right hand, spatial positions */            \
A80, A70, A60, A50, A40, A30, A20,                      B20, B30, B40, B50, B60, B70, B80,\
A81, A71, A61, A51, A41, A31, A21,                      B21, B31, B41, B51, B61, B71, B81,\
A82, A72, A62, A52, A42, A32,      A13, A03,  B03, B13,      B32, B42, B52, B62, B72, B82,\
A83, A73, A63, A53, A43, A33, A23,      A04,  B04,      B23, B33, B43, B53, B63, B73, B83,\
A84, A74, A64, A54, A44,      A34, A24, A14,  B14, B24, B34,      B44, B54, B64, B74, B84)\
   /* matrix positions */                \
 {                                       \
    { KC_NO, KC_NO, KC_NO, A03,   A04 }, \
    { KC_NO, KC_NO, KC_NO, A13,   A14 }, \
    { A20,   A21,   KC_NO, A23,   A24 }, \
    { A30,   A31,   A32,   A33,   A34 }, \
    { A40,   A41,   A42,   A43,   A44 }, \
    { A50,   A51,   A52,   A53,   A54 }, \
    { A60,   A61,   A62,   A63,   A64 }, \
    { A70,   A71,   A72,   A73,   A74 }, \
    { A80,   A81,   A82,   A83,   A84 }, \
    { KC_NO, KC_NO, KC_NO, B03,   B04 }, \
    { KC_NO, KC_NO, KC_NO, B13,   B14 }, \
    { B20,   B21,   KC_NO, B23,   B24 }, \
    { B30,   B31,   B32,   B33,   B34 }, \
    { B40,   B41,   B42,   B43,   B44 }, \
    { B50,   B51,   B52,   B53,   B54 }, \
    { B60,   B61,   B62,   B63,   B64 }, \
    { B70,   B71,   B72,   B73,   B74 }, \
    { B80,   B81,   B82,   B83,   B84 }  \
}

#define LAYOUT_ergodox_wrapper(...) KEYMAP_custom(__VA_ARGS__) // to expand base layout



const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {
/*
 * left hand
 *    +-------+-----+-----+-----+-----+-----+-----+
 *    |             name_LEFT_NB            |  k1 |
 *    +-------+-----+-----+-----+-----+-----+-----+
 *    |             name_LEFT_01            |  k2 |
 *    +-------+-----+-----+-----+-----+-----+     |          +-----+-----+
 *    |             name_LEFT_02            +-----+          | k4  |  k5 |
 *    +-------+-----+-----+-----+-----+-----+  k3 |    +-----+-----+-----+
 *    |             name_LEFT_03            |     |    |     |     |  k6 |
 *    +-+-----+-----+-----+-----+-----+-----+-----+    |    THl2   +-----+
 *      | k11 | k10 |  k9 | k8  | THl1|                |     |     |  k7 |
 *      +-----+-----+-----+-----+-----+                +-----+-----+-----+
 *                                 * name_LEFT_TH split in TH1 and TH2
 *
 * Symetrical for right hand...
 */

#define _LAYER_(name, k1, k2, k3, k4, k5, k6, k7, k8, k9, k10,k11,             \
                      k12,k13,k14,k15,k16,k17,k18,k19,k20,k21,k22)             \
  [name] = LAYOUT_ergodox_wrapper(                                             \
       name##_LEFT_NB, k1,                          k12, name##_RIGHT_NB,      \
       name##_LEFT_01, k2,                          k13, name##_RIGHT_01,      \
       name##_LEFT_02,     k4, k5,       k16, k15,       name##_RIGHT_02,      \
       name##_LEFT_03, k3,      k6,      k17,       k14, name##_RIGHT_03,      \
 k11,k10,k9,k8, name##_LEFT_TH, k7,      k18, name##_RIGHT_TH, k19,k20,k21,k22)

_LAYER_(BEPO, KC_ESC, KC_ENT, KC_LSFT,
              KC_LALT, KC_LGUI, KC_HOME, KC_END,
              xxxxxxx, CUT, PASTE, COPY,
              BP_PERC, KC_BSPC, KC_LSFT,
              TG(ADJUST), xxxxxxx, KC_PGUP, KC_PGDN,
              xxxxxxx, xxxxxxx, xxxxxxx, BP_CCED),

_LAYER_(RAISE, _______, _______, _______,
              _______, _______, _______, _______,
              _______, _______, _______, _______,

              _______, _______, _______,
              _______, _______, _______, _______,
              _______, _______, _______, _______),

_LAYER_(LOWER, _______, _______, _______,
              _______, _______, _______, _______,
              _______, _______, _______, _______,

              _______, _______, _______,
              _______, _______, _______, _______,
              _______, _______, _______, _______),

_LAYER_(ADJUST, _______, _______, _______,
              _______, _______, _______, _______,
              _______, _______, _______, _______,

              _______, _______, _______,
              _______, _______, _______, _______,
              _______, _______, _______, _______),

_LAYER_(FN,   _______, _______, _______,
              _______, _______, _______, _______,
              _______, _______, _______, _______,

              _______, _______, _______,
              _______, _______, _______, _______,
              _______, _______, _______, _______),

_LAYER_(GAME, _______, _______, _______,
              _______, _______, _______, _______,
              _______, _______, _______, _______,

              _______, _______, _______,
              _______, _______, _______, _______,
              _______, _______, _______, _______),
};


// Runs just one time when the keyboard initializes.
void matrix_init_user(void) {
  //lcd_backlight_hal_init();
  //lcd_backlight_brightness(255);
}
