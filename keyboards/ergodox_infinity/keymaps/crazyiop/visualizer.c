#include "simple_visualizer.h"
#include "util.h"
#include "crazyiop.h"

static void get_visualizer_layer_and_color(visualizer_state_t* state) {
  if (host_keyboard_leds() & (1<<USB_LED_CAPS_LOCK)) {
    state->layer_text = "Capslock";
    state->target_lcd_color = LCD_COLOR(0, 0, 255); // white
  } else {
    switch (biton32(layer_state)) {
      case RAISE:
        state->layer_text = "Raise";
        state->target_lcd_color = LCD_COLOR(0, 255, 255); // red
        break;
      case LOWER:
        state->layer_text = "Lower";
        state->target_lcd_color = LCD_COLOR(135, 255, 255); // blue
        break;
      case ADJUST:
        state->layer_text = "Adjust";
        state->target_lcd_color = LCD_COLOR(95, 255, 255); // green
        break;
      case GAME:
        state->layer_text = "Adjust";
        state->target_lcd_color = LCD_COLOR(46, 255, 255); // yellow
        break;
      case BEPO:
      default:
        state->layer_text = "";
        state->target_lcd_color = LCD_COLOR(0, 0, 0);
        break;
    }
  }
}
