#include "preonic.h"
#include "action_layer.h"
#include "keymap_bepo.h"
#include <sendstring_bepo.h>
#include "crazyiop.h"


const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {
/* preonic layout helper
 * ,------+------+------+------+------+------+------+------+------+------+------+------.
 * |             name_LEFT_NB                |             name_RIGHT_NB               |
 * |------+------+------+------+------+------+------+------+------+------+------+------|
 * |             name_LEFT_01                |             name_RIGHT_01               |
 * |------+------+------+------+------+-------------+------+------+------+------+------|
 * |             name_LEFT_02                |             name_RIGHT_02               |
 * |------+------+------+------+------+------|------+------+------+------+------+------|
 * |             name_LEFT_03                |             name_RIGHT_03               |
 * |------+------+------+------+------+------+------+------+------+------+------+------|
 * |  k1  |  k2  |  k3  |  name_LEFT_TH      |    name_RIGHT_TH   |  k4  | k5   |  k6  |
 * `------+------+------+------+------+------+------+------+------+------+------+------'
 */
#define _LAYER_(name, k1, k2, k3,k4,k5,k6)                     \
  [name] = {                                                   \
    {            name##_LEFT_NB, name##_RIGHT_NB            }, \
    {            name##_LEFT_01, name##_RIGHT_01            }, \
    {            name##_LEFT_02, name##_RIGHT_02            }, \
    {            name##_LEFT_03, name##_RIGHT_03            }, \
    {k1, k2, k3, name##_LEFT_TH, name##_RIGHT_TH, k4, k5, k6}, \
  }

  _LAYER_(BEPO,   COPY,    PASTE,   xxxxxxx,    xxxxxxx, xxxxxxx, BP_CCED),
  _LAYER_(LOWER,  _______, _______, _______,    _______, _______, _______),
  _LAYER_(RAISE,  _______, _______, _______,    _______, _______, _______),
  _LAYER_(ADJUST, _______, _______, _______,    _______, _______, _______),
  _LAYER_(FN,     _______, _______, _______,    _______, _______, _______),
  _LAYER_(GAME,   _______, _______, _______,    _______, _______, _______),
};


// Hardware specific functions

static int8_t led_level = 2;

void matrix_init_user(void) {
  _delay_ms(20);
  rgblight_enable();
};

void cycle_led_brigthness(void)
{
  led_level++;
}

void set_led_color(color_e color)
{
  switch (color)
  {
    case OFF:
      rgblight_sethsv(0, 0, 0);
      break;
    case WHITE:
      rgblight_sethsv(0, 0, led_level * 193 / (LED_LEVEL + 1)); // white
      break;
    case RED:
      rgblight_sethsv(0, 255, led_level * 193 / (LED_LEVEL + 1)); // red
      break;
    case BLUE:
      rgblight_sethsv(191, 247, led_level * 193 / (LED_LEVEL + 1)); // blue
      break;
    case GREEN:
      rgblight_sethsv(135, 240, led_level * 193 / (LED_LEVEL + 1)); // green
      break;
    case YELLOW:
      rgblight_sethsv(65, 234, led_level * 193 / (LED_LEVEL + 1)); // yellow
      break;
  }
}
