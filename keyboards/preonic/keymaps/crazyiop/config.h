#ifndef CONFIG_USER_H
#define CONFIG_USER_H

#include "config_common.h"

#define PERMISSIVE_HOLD
#undef  IGNORE_MOD_TAP_INTERRUPT

#undef  RGB_DI_PIN
#define RGB_DI_PIN B2

#undef  RGBLED_NUM
#define RGBLED_NUM 8

// save flash
#define NO_DEBUG
#define NO_PRINT
#endif
